package example.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@EqualsAndHashCode(callSuper = true)
@Table(name = "dog")
@Data
public class Dog extends AbstractEntity {
    private String name;
    private Boolean barks;

    @OneToOne(mappedBy = "dog")
    private Person person;
}
